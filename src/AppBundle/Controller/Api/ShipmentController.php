<?php

namespace AppBundle\Controller\Api;

use AppBundle\Form\Type\ItemType;
use AppBundle\Form\Type\ShipmentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Api controller fo shipment
 *
 * @Route("api/shipment/")
 */
class ShipmentController extends Controller
{
    private const SHIPMET_URI = "shipment";
    private const ITEM_URI = "item";

    /**
     * @param $res
     * @param $errors
     * @return null|string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function checkForError($res, $errors)
    {
//        TODO: move to service
        if (isset($res['error'])) {
            if ($res['error']['code'] === Response::HTTP_UNAUTHORIZED) {
                throw new AccessDeniedException($res['error']['mes']);
            }
            $arrError = json_decode($res['error']['mes'], true);
            $error = $arrError['message'];
            $errors[] = $error;
        }

        return $errors;
    }

    /**
     * @Route("", name="api_index_shipment")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $apiClient = $this->get('app.service.api_client');
        $form = $this->createForm(ShipmentType::class);
        $form->handleRequest($request);

        $errors = [];
        if ($form->isValid() && $form->isSubmitted()) {
            $res = $apiClient->sendPost(self::SHIPMET_URI,
                ['id' => $form->get('id')->getData(), 'name' => $form->get('name')->getData()]
            );
            $errors = $this->checkForError($res, $errors);
        }
        $shipments = $apiClient->sendGet(self::SHIPMET_URI);
        $errors = $this->checkForError($shipments, $errors);

        $formItem = $this->createForm(ItemType::class);
        $choiceArr = [];
        foreach ($shipments['shipments'] as $ship) {
            $choiceArr[$ship['name']] = $ship['id'];
        }
        $formItem->add('shipping', ChoiceType::class, ['choices' => $choiceArr]);
        $formItem->handleRequest($request);

        if ($formItem->isValid() && $formItem->isSubmitted()) {
            $item = $apiClient->sendPost(self::ITEM_URI,
                ['id' => $formItem->get('id')->getData(), 'shipment_id' => $formItem->get('shipping')->getData(),
                    'name' => $formItem->get('name')->getData(), 'code' => $formItem->get('code')->getData()]
            );
            $errors = $this->checkForError($item, $errors);
            return $this->redirectToRoute('api_index_shipment');
        }

        return $this->render('shipment/index.html.twig', array(
            'shipments' => $shipments['shipments'],
            'form' => $form->createView(),
            'formItem' => $formItem->createView(),
            'error' => $errors
        ));
    }

    /**
     * @Route("edit/{id}", name="api_edit_shipment")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function editAction(Request $request, $id): Response
    {
        $apiClient = $this->get('app.service.api_client');
        $errors = [];
        $shipment = $apiClient->sendGet(self::SHIPMET_URI, (int)$id);
        $errors = $this->checkForError($shipment, $errors);

        $form = $this->createForm(ShipmentType::class);
        $form->get('id')->setData((int)$id);
        $form->get('name')->setData($shipment['name']);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $res = $apiClient->sendPut(self::SHIPMET_URI,
                ['id' => $form->get('id')->getData(), 'name' => $form->get('name')->getData()],
                $id
            );
            $errors = $this->checkForError($res, $errors);

            return $this->redirectToRoute('api_index_shipment');
        }
        $shipments = $apiClient->sendGet(self::SHIPMET_URI);
        $errors = $this->checkForError($shipments, $errors);

        return $this->render('shipment/edit.html.twig', array(
            'form' => $form->createView(),
            'error' => $errors
        ));
    }

    /**
     * @Route("delete/{id}", name="api_delete_shipment")
     * @param $id
     * @return Response
     * @internal param Request $request
     */
    public function deleteAction($id): Response
    {
        $apiClient = $this->get('app.service.api_client');
        $errors = [];
        $shipments = $apiClient->sendDelete(self::SHIPMET_URI, $id);
        $errors = $this->checkForError($shipments, $errors);
        return $this->redirectToRoute('api_index_shipment');
    }

    /**
     * @Route("send/{id}", name="api_send_shipment")
     * @param $id
     * @return Response
     * @internal param Request $request
     */
    public function sendAction($id): Response
    {
        $apiClient = $this->get('app.service.api_client');
        $errors = [];
        $shipments = $apiClient->sendPost(self::SHIPMET_URI . '/' . $id . '/send', ['id' => $id]);
        $errors = $this->checkForError($shipments, $errors);
        return $this->redirectToRoute('api_index_shipment');
    }
}
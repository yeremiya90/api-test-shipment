<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @param Request $request
     *
     * @Route("/token", name="token")
     * @return RedirectResponse|Response
     */
    public function tokenAction(Request $request)
    {
        try {
            $cache = new FilesystemCache();
            $client = $this->container->get('eight_points_guzzle.client.api_shipment');
            $user = $this->getUser();

            $email = $user->getUsername();
            $password = $user->getPassword();
            $response = $client->post('/login', ['json' => ['email' => $email, 'password' => $password]]);
            $jsonBody = $response->getBody()->getContents();
            $arr = json_decode($jsonBody, true);
            $token = $arr['data'][0]['token'];
            $cache->set('shipment_token', $token);
            return $this->redirectToRoute('api_index_shipment');
        } catch (Exception $e) {
            return $this->render('security/login.html.twig', [
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    }
}
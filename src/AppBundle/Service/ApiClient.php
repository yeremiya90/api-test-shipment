<?php

namespace AppBundle\Service;

use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApiClient
 * @package AppBundle\Service
 */
class ApiClient
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @var string
     */
    private $authorization;

    /** @var
     * GuzzleHttp\Client $client
     */
    private $client;

    /**
     * ApiClient constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->cache = new FilesystemCache();
        $this->preConfig();
    }

    /**
     * function for prepare authorization and Guzzle client
     */
    private function preConfig(): void
    {
        $token = $this->cache->get('shipment_token');
        $this->authorization = 'bearer ' . $token;
        $this->client = $this->container->get('eight_points_guzzle.client.api_shipment');
    }

    /**
     * @param $uri
     * @param bool $id
     * @return array
     */
    public function sendGet($uri, $id = false): array
    {
        if ($id) {
            $uri = $uri . '/' . $id;
        }
        try {
            $response = $this->client->get($uri, [
                'headers' => ['Authorization' => $this->authorization]
            ]);
        } catch (RequestException  $e) {
            return ['error' => ['code' => $e->getCode(), 'mes' => $e->getResponse()->getBody()->getContents()]];
        }
        $jsonBody = $response->getBody()->getContents();
        $arr = json_decode($jsonBody, true);

        return $arr['data'];
    }

    /**
     * @param $uri
     * @param $arrParam
     * @return array
     */
    public function sendPost($uri, $arrParam): ?array
    {
        try {
            $response = $this->client->post($uri, [
                'headers' => ['Authorization' => $this->authorization],
                'json' => $arrParam
            ]);
        } catch (RequestException  $e) {
            return ['error' => ['code' => $e->getCode(), 'mes' => $e->getResponse()->getBody()->getContents()]];
        }
        $jsonBody = $response->getBody()->getContents();
        $arr = json_decode($jsonBody, true);

        return $arr['data'];
    }

    /**
     * @param $uri
     * @param $arrParam
     * @param $id
     * @return mixed
     */
    public function sendPut($uri, $arrParam, $id): ?array
    {
        try {
            $uri = $uri . '/' . $id;
            $response = $this->client->put($uri, [
                'headers' => ['Authorization' => $this->authorization],
                'json' => $arrParam
            ]);
        } catch (RequestException $e) {
            return ['error' => ['code' => $e->getCode(), 'mes' => $e->getResponse()->getBody()->getContents()]];
        }
        $jsonBody = $response->getBody()->getContents();
        $arr = json_decode($jsonBody, true);

        return $arr['data'];
    }

    /**
     * @param $uri
     * @param $id
     * @return array|mixed|null
     * @internal param $arrParam
     */
    public function sendDelete($uri, $id): ?array
    {
        try {
            $uri = $uri . '/' . $id;
            $response = $this->client->delete($uri, [
                'headers' => ['Authorization' => $this->authorization]
            ]);
        } catch (RequestException  $e) {
            return ['error' => ['code' => $e->getCode(), 'mes' => $e->getResponse()->getBody()->getContents()]];
        }
        $jsonBody = $response->getBody()->getContents();
        $arr = json_decode($jsonBody, true);

        return $arr['data'];
    }
}